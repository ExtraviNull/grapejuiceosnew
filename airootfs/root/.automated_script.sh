#!/usr/bin/env bash
pacman -U ./cinnamon-5.0.4-1-x86_64.pkg.tar.zst
pacman -U ./gdm-40.0-1-x86_64.pkg.tar.zst
pacman -U ./xf86-input-synaptics-1.9.1-2-x86_64.pkg.tar.zst

systemctl stop dhcpcd@ens33.service
systemctl disable dhcpcd@ens33.service
systemctl stop dhcpcd.service
systemctl disable dhcpcd.service
systemctl start NetworkManager
systemctl enable NetworkManager
systemctl enable gdm
systemctl start gdm
script_cmdline ()
{
    local param
    for param in $(< /proc/cmdline); do
        case "${param}" in
            script=*) echo "${param#*=}" ; return 0 ;;
        esac
    done
}

automated_script ()
{
    local script rt
    script="$(script_cmdline)"
    if [[ -n "${script}" && ! -x /tmp/startup_script ]]; then
        if [[ "${script}" =~ ^((http|https|ftp)://) ]]; then
            curl "${script}" --location --retry-connrefused -s -o /tmp/startup_script >/dev/null
            rt=$?
        else
            cp "${script}" /tmp/startup_script
            rt=$?
        fi
        if [[ ${rt} -eq 0 ]]; then
            chmod +x /tmp/startup_script
            /tmp/startup_script
        fi
    fi
}

if [[ $(tty) == "/dev/tty1" ]]; then
    automated_script
fi
